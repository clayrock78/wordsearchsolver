from re import X
import turtle as trtl
from wordsearch_words import Game
from wordsearch_solver import main as solver
from random import choice
from color_constants import COLORS as ADVANCED_COLORS
import math

trtl.colormode(255)

class Writer(trtl.Turtle):
    def __init__(self):
        super().__init__()

        # attributes of the window
        self.window_width = 11 * 50
        self.window_height = 14 * 50
        self.TOP = self.window_height / 2
        self.BOTTOM = self.window_height / -2
        self.LEFT = self.window_width / -2
        self.RIGHT = self.window_width / 2

        # attributes of the turtle
        self.speed(10)
        self.pensize(5)
        self.ht()
        self.pu()

    def set_box_dimensions(self, dimensions):
        self.box_dimensions = (self.window_width / dimensions[1], self.window_height / dimensions[0])

    def write_text(self, text) -> None:
        self.write(text, align='center', font=("Verdana", 15, "normal"))

    def move_right(self, units) -> None:
        " Moves right without changing the heading of the turtle. "
        self.right(90)
        self.forward(units)
        self.left(90)
    
    def move_left(self, units) -> None:
        " Moves left without changing the heading of the turtle. "
        self.left(90)
        self.forward(units)
        self.right(90)
    
    def grid_to_cartesian(self, pos: tuple) -> tuple:
        return ((pos[0] * self.box_dimensions[0] + 25) + self.LEFT, ((-pos[1] - 1) * self.box_dimensions[1] + 25) + self.TOP)

    def direction_to_angle(self, direction) -> float:
        return math.degrees(math.atan2(-direction[1], direction[0]))

    def draw_grid(self, game) -> None:
        " Draws the grid. "
        self.set_box_dimensions(game.dimensions)
        self.goto(self.LEFT, self.BOTTOM)
        self.pd()
        # draw the horizontal lines
        self.setheading(90)
        for _ in range(game.dimensions[1]):
            self.forward(self.window_height)
            self.backward(self.window_height)
            self.move_right(self.box_dimensions[0])
        self.forward(self.window_height)
        
        #  draw the vertical lines
        self.setheading(180)
        for _ in range(game.dimensions[0]):
            self.forward(self.window_width)
            self.backward(self.window_width)
            self.move_left(self.box_dimensions[1])
        self.pu()
        trtl.update()
    
    def write_letters(self, game: Game) -> None:
        " Writes the letters to the board. "
        box_dimensions = (self.window_width / game.dimensions[1], self.window_height / game.dimensions[0])
        self.goto(self.LEFT + box_dimensions[0] / 2, self.TOP - box_dimensions[1] / (4/3))
        self.setheading(90)
        for line in game.board:
            for word in line:
                self.write_text(word)
                self.goto(self.xcor() + box_dimensions[0], self.ycor())
            self.goto(self.LEFT + box_dimensions[0] / 2, self.ycor() - box_dimensions[1])
    
    def draw_solution(self, game: Game) -> None:
        " Draws all solutions to the game. "
        solutions = solver(game.board, game.words_to_find)
        self.hideturtle()
        trtl.tracer(True)
        self.speed(3)
        # iterate over all solutions
        for key in solutions:
            color = (255,255,255)
            while sum(color) > 650: # Rejection sampling to avoid light colors
                color = ADVANCED_COLORS[choice(list(ADVANCED_COLORS.keys()))]
            
            self.color(color)
            # get the start and end position of that solution
            sx, sy, ex, ey = solutions[key]
            start_point = self.grid_to_cartesian((sx,sy))
            end_point = self.grid_to_cartesian((ex,ey))

            # set angle for the arrow
            #new_angle = math.degrees(math.atan2((sy-ey),(sx-ex)))
            
            # draw a line from the start to the end
            self.goto(start_point)
            self.pd()
            self.goto(end_point)
            self.pu()
