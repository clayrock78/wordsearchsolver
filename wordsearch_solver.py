"""
Word search solver, kinda garbage until we get something to scrape the screen for the crossword board
"""

# words_to_find_dict = { word[0]: word[1:] for word in words_to_find }

"""
indexes are y, x from the top left corner, starting from 0

y-downwards is bigger y
x-right is bigger x
"""
from wordsearch_words import DIRECTIONS_TO_WORDS


def main(board: list, words_to_find: list) -> dict:

   
    # TODO it is possible to increase the efficiency of the algorithm by
    # adding the ability to check for each word in one pass of the board
    # as opposed to one pass of the board for each word

    solutions = dict()

    for word_num, word in enumerate(words_to_find):
        for y, line in enumerate(board):
            for x, character in enumerate(line):
                if character == word[0]:
                    # check for the word in each direction
                    for dx in [-1, 0, 1]:
                        for dy in [-1, 0, 1]:
                            found = True
                            # iterate through letters  of the word, breaking if we dont find what we want
                            for i, c in enumerate(word):
                                index_y = y + dy * i
                                index_x = x + dx * i

                                # dont look at letters off the board
                                if (
                                    len(board) - 1 < index_y
                                    or index_y < 0
                                    or 0 > index_x
                                    or index_x > len(board[0]) - 1
                                ):
                                    found = False
                                    break

                                square = board[index_y][index_x]

                                # break if we dont find what we want
                                if square != word[i]:
                                    found = False
                                    break

                            if found:
                                direction_in_words = DIRECTIONS_TO_WORDS[f"{dy}, {dx}"]
                                print(
                                    f'word "{word}" ({word_num}) found at {x+1,y+1} ({direction_in_words})'
                                )
                                solutions[word] = (x,y, x + dx * i  ,y + dy * i)
                                break
    
    return solutions

