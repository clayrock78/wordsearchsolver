"""
Collection of crossword puzzles
"""

DIRECTIONS_TO_WORDS = {
    "-1, -1": "up and left",
    "-1, 0": "up",
    "-1, 1": "up and right",
    "0, -1": "left",
    "0, 1": "right",
    "1, -1": "down and left",
    "1, 0": "down",
    "1, 1": "down and right",
}

class Game():
    def __init__(self, crossword_board, words_to_find):
        self.crossword_board = crossword_board
        self.words_to_find = words_to_find
        self.board = [line.split() for line in self.crossword_board.split("\n")]
        self.dimensions = (len(self.board), len(self.board[0]))
        

CHEMISTRY_GAME = Game(
        crossword_board="""\
        G O N H Y D R O G E N R
        E N I M O R B A G I H O
        M U I L E H A N D R I L
        N E O N K R Y P T O N E
        L P C H L O R I N E N A
        R O A S T A T I N E N D
        E R E O X Y G E N E D Y
        I T N U P N N R G N N E
        O L I C C O E O L O N X
        D O R H N A R A R G H E
        I X O G P T R O E R N N
        N R U E I A B B E A C O
        E B L N T L R Y O N Y N
        I N F T N H G G R N N T\
        """,
        words_to_find=[
            "NITROGEN",
            "CARBON",
            "IODINE",
            "GRAPHITE",
            "BORON",
            "HELIUM",
            "FLOURINE",
            "BROMINE",
            "LEAD",
            "RADON",
            "ARGON",
            "XENON",
            "OXYGEN",
            "HYDROGEN",
            "CHLORINE",
            "NEON",
            "ASTATINE",
            "KRYPTON",
            "ANDREA"
        ]
        
)