import turtle as trtl
from wordsearch_words import *
import writer
  
# setup window
trtl.tracer(False)

game = CHEMISTRY_GAME
print(game.board)
print(game.word_search_board)
print(game.dimensions)
writer = writer.Writer() # OOP be like
writer.draw_grid(game)
writer.write_letters(game)
writer.draw_solution(game)

trtl.mainloop()