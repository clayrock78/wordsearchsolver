# WordsearchSolver

A word search solver that I made.

## Description
It's pretty self-explanatory, but this program solves word searches, I also build some code around it to display the solutions (and admittedly got a little carried away). I had also started working on a system to webscrape wordsearches and extract the data from the downloaded images, but lost interest after the webscraping part.

## Usage
Pretty simple, the main file to run is wordsearch_displayer.py. Lacking any real usability as of currently.

## Project status
I'm likely entirely done with this project. I may revisit it in the future to do the data extraction from webscraped PNGs of wordsearches.
